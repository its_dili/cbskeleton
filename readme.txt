The intentions of this skeleton are
- minimize the files placed into the webroot
- without using of external configured mappings
- without session management
- without default layouts and views
- protect app lifecycle actions

run:
====
box install
box start
