component {

    function configure() {
        coldbox = {
            reinitPassword = '6q432c56bn64iwrntreztdr'
            ,applicationStartHandler = 'App.onApplicationStart'
        };
        environments = {
            // The key is the name of the environment
            // The value is a list of regex to match against cgi.http_host
            development = 'localhost,127.0.0.1'
        };
        layoutSettings = {
            defaultLayout = ''
        };
        flash = {
            // default is 'session', but does not work with 'this.sessionManagement=false'
            // but why not? there is a check in CB in RequestService.buildFlashScope()
            // isn't it used/working?
            scope = 'cache'
        };
    }

    function development() {
        coldbox.customErrorTemplate = '/coldbox/system/includes/BugReport.cfm';
        coldbox.handlerCaching = false;
        coldbox.eventCaching = false;
        coldbox.viewCaching = false;
        coldbox.reinitPassword = '';
        coldbox.handlersIndexAutoReload = true;
    }

}
