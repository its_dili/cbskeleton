/**
 * @hint This component contains the mapped application lifecycle methods. The methods are protected against direct access via URL. The methods itself cannot be secured by declaration as 'private', otherwise ColdBox could not execute the methods.
 */
component {
    componentName = getMetadata( this ).name.listLast( '.' );

    function onApplicationStart( event, rc, prc ) {
        protectDirectExecution( rc, getFunctionCalledName() );
        /* write your code here */
    }

    /**
     * @hint This function compares the requested event against called method to prevent direct URL access
     */
    private void function protectDirectExecution( rc, calledFunctionName ) {
        var rcEvent = rc.keyExists( 'event' ) ? rc.event : '';
        if ( rcEvent.listLen( '.' ) LT 2) return; // unknown event
        // remove modules/packages from rcEvent
        while ( rcEvent.listLen( '.' ) GT 2 ) {
            rcEvent = rcEvent.listRest( '.' );
        }
        if (
            compareNoCase( rcEvent.listFirst( '.' ), componentName ) == 0
            &&
            compareNoCase( rcEvent.listLast( '.' ), calledFunctionName ) == 0
        ) {
            throw( componentName & '.' & calledFunctionName & ' is protected' );
        }
    }

}
