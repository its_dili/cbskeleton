component {

    this.name = hash( getCurrentTemplatePath() );
    this.sessionManagement = true;

    webroot = getDirectoryFromPath( getCurrentTemplatePath() );
    projectroot = getCanonicalPath( webroot & '../' ) & '/'; // one level up
    approot = projectroot & 'src/app/';
    this.mappings['/'] = approot;
    this.mappings['/coldbox'] = projectroot & 'src/coldbox/';
    this.mappings['/projectroot'] = projectroot;
    this.mappings['/webroot'] = webroot;

    function onApplicationStart(){
        application.cbBootstrap = new coldbox.system.Bootstrap(
            COLDBOX_CONFIG_FILE     = 'config.Coldbox'
            ,COLDBOX_APP_ROOT_PATH  = approot
        );
        return application.cbBootstrap.onApplicationStart();
    }

    function onApplicationEnd( ApplicationScope ) {
        application.cbBootstrap.onApplicationEnd( ApplicationScope );
    }

    function onSessionStart() {
        application.cbBootstrap.onSessionStart();
    }

    function onSessionEnd( SessionScope, ApplicationScope) {
        application.cbBootstrap.onSessionEnd( SessionScope, ApplicationScope );
    }

    function onRequestStart( targetPage ) {
        return application.cbBootstrap.onRequestStart( targetPage );
    }

    function onMissingTemplate( targetPage ) {
        return application.cbBootstrap.onMissingTemplate( targetPage );
    }

}